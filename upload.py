import requests
import os
import json
from tqdm import tqdm



# URL of your Label Studio instance and ID of your project
label_studio_url = "http://localhost:8080"
project_id = 15  # Replace with your project ID
api_token = "fcdf5a15c2dddf9412511c5b7f7a14c647fd97fc"

# Directory containing your images and JSON files
data_directory =  "/Users/aymejri/quant_code/experiments/add_boxes_candidats/dataset/training_data/images"

# Get a list of all image files in the directory
image_files = [f for f in os.listdir(data_directory) if f.endswith(('.png', '.jpg'))]

# For each image file, upload the image and its corresponding JSON file
for image_file in tqdm(image_files):
    image_path = os.path.join(data_directory, image_file)
    json_file = image_file.rsplit('.', 1)[0] + '.json'
    json_path = os.path.join( "/Users/aymejri/quant_code/experiments/add_boxes_candidats/dataset/training_data/lbl_annotations_v2", json_file)

    # Open the image and JSON file
    image_data = open(image_path, "rb").read()
    json_data = json.load(open(json_path))

    # Prepare the data for the API request
    data = {
        'file': (image_file, image_data),
        'data': (json_file, json.dumps(json_data), 'application/json')
    }

    
    headers = {
    "Authorization": f"Token {api_token}",
    "Content-Type": "application/json"
    }

    # Send the API request to create a new task in the project
    response = requests.post(f"{label_studio_url}/api/projects/{project_id}/tasks/bulk",
                            headers=headers,
                            json=json_data)

    # Check the response
    if response.status_code == 201:
        print(f"Successfully uploaded {image_file} and {json_file}.")
    else:
        print(f"Failed to upload {image_file} and {json_file}. Response: {response.text}")
