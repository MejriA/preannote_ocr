import os
import cv2
import json
from PIL import Image
import pytesseract
from pytesseract import Output
from tqdm import tqdm

def extract_text(image_path):
    # Load the image from file
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Use pytesseract to extract text and bounding box
    results = pytesseract.image_to_data(gray, output_type=Output.DICT)

    # Store the bounding box coordinates and corresponding text
    boxes = []
    for i in range(len(results["text"])):
        if float(results["conf"][i]) > 0:
            (x, y, w, h) = (results["left"][i], results["top"][i], results["width"][i], results["height"][i])
            text = results["text"][i]
            boxes.append({
                'x': x,
                'y': y,
                'w': w,
                'h': h,
                'text': text,
            })

    return boxes

def process_images(input_folder, output_folder):
    # Ensure output folder exists
    os.makedirs(output_folder, exist_ok=True)

    # Iterate over all images in the input folder
    for filename in tqdm(os.listdir(input_folder)):
        if filename.endswith('.jpg') or filename.endswith('.png'):  # Add more formats if needed
            print(f'Processing {filename}...')
            image_path = os.path.join(input_folder, filename)

            # Extract text and bounding boxes
            boxes = extract_text(image_path)

            # Prepare output for Label Studio in the format:
            # {"image": "image_path", "annotations": [{"bbox": {"top": y, "left": x, "height": h, "width": w}, "result": [{"value": {"text": [text]}}]}]}
            output = {
                'image': os.path.join(input_folder, filename),
                'annotations': [{'bbox': {'top': box['y'], 'left': box['x'], 'height': box['h'], 'width': box['w']},
                                 'result': [{'value': {'text': [box['text']]}}]} for box in boxes]
            }

            # Save output to a JSON file
            output_path = os.path.join(output_folder, f'{os.path.splitext(filename)[0]}.json')
            with open(output_path, 'w') as outfile:
                json.dump(output, outfile, indent=4)

process_images( "/Users/aymejri/quant_code/experiments/add_boxes_candidats/dataset/training_data/images",  "/Users/aymejri/quant_code/experiments/add_boxes_candidats/dataset/training_data/lbl_annotations_v2")
